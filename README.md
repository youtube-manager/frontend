# Youtube Video Manager: Frontend

A frontend part of Youtube Video Manager web-service.

## Deployment

To deploy project run:
1. Execute `docker-compose -f deploy/docker-compose build`
2. Execute `docker-compose -f deploy/docker-compose up`

... or, if you have [Node.js](https://nodejs.org/en/) & [npm](https://www.npmjs.com/) installed, run:
1. Execute `npm install`
2. Execute `npm run build`
3. Execute `npm run start`
