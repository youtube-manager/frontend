import { APIURL } from './settings.js';
import { user } from './stores.js';


/*
 * Sends request to project's REST API
 *  @param(url) - relative API URL
 *  @param(options) - request options, such as headers, body, method etc
 *
 * Automatically includes Content-Type: application/json header
 * Automatically includes Authorization: Token header
 */
export async function sendRequest(url, options) {
    if (!options) {
        options = {};
    }

    if (!options.headers)
        options.headers = {};

    if (!options.headers['Content-Type'])
        options.headers['Content-Type'] = 'application/json';

    let userData;
    user.subscribe(data => userData = data);

    if (!user.isEmpty()) {
        options.headers['Authorization'] = `Token ${userData.token}`;
    }

    return await fetch(`${APIURL}${url}`, options);
}

// Gets current logged in user data from API
export async function getUserData(token) {
    console.log(token);
    const response = await fetch(`${APIURL}/users/current/`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${token}`
        }
    })

    if (response.ok) {
        return await response.json();
    }

    return null;
}

// Logges user ouut
export function logout() {
    user.set({});
}

// Shows success action GREEN toast (message)
export function raiseSuccessToast(text) {
    M.toast({
        html: text,
        classes: 'green darken-2 white-text'
    });
}

// Shows failure action RED toast (message)
export function raiseFailureToast(text) {
    M.toast({
        html: text,
        classes: 'red darken-2 white-text'
    });
}

// Formats video publish date according to project setup
export function formatPublishDate(date) {
    const publishDate = new Date(date);
    const formatOptions = {
        day: '2-digit',
        month: 'short',
        year: 'numeric',
    };

    return publishDate.toLocaleDateString('en-US', formatOptions);
}
