import { writable } from "svelte/store";

// Function to create a svelte's writable store
// that uses localStorage for data
const createWritableStore = (key, startValue) => {
    const { subscribe, set, update } = writable(startValue);

    const json = localStorage.getItem(key);
    if (json) {
        set(JSON.parse(json));
    }

    subscribe(current => {
        localStorage.setItem(key, JSON.stringify(current));
    });

    // Checks whether the user object is empty
    const isEmpty = () => {
        if (json) {
        let obj = JSON.parse(json);
        return Object.keys(obj).length === 0 && obj.constructor === Object;
        } else {
        return false;
        }
    }

    return {
        subscribe,
        set,
        update,
        isEmpty
    };
}

// Store for user data got from API
export let user = createWritableStore('user', {});
